package br.com.caelum.financas.modelo;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static javax.persistence.EnumType.STRING;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
public class Movimentacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;

    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime data;

    @ManyToOne
    private Conta conta;
    private BigDecimal valor;

    @Enumerated(STRING)
    private TipoMovimentacao tipoMovimentacao;

    public Movimentacao(Long id, String descricao, LocalDateTime data, Conta conta, BigDecimal valor, TipoMovimentacao tipoMovimentacao) {
        this.id = id;
        this.descricao = descricao;
        this.data = data;
        this.conta = conta;
        this.valor = valor;
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public Movimentacao() {}

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    @Override
    public String toString() {
        return "Movimentacao{" +
                "id=" + id +
                ", descricao='" + descricao + '\'' +
                ", data=" + data +
                ", conta=" + conta +
                ", valor=" + valor +
                ", tipoMovimentacao=" + tipoMovimentacao +
                '}';
    }
}
